
from django.conf.urls import url
from django.contrib import admin
from django.conf.urls import include
from django.conf import settings
from app_phva import views
from django.conf.urls.static import static

urlpatterns = [
    # (r'^accounts/', include('registration.backends.default.urls')),
    url(r'^admin/', admin.site.urls),
    #url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^', include('app_phva.urls')),

]