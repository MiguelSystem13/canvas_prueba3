# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from app_phva.models import *

def index(request):
	if request.method == "POST" and 'btn_planear' in request.POST:
		Phva.objects.filter(id_proyecto = 1).update(p_que_se_hara = request.POST['p_que_se_hara'], p_como = request.POST['p_como'], p_cuando = request.POST['p_cuando'], p_donde = request.POST['p_donde'], p_quien = request.POST['p_quien'], p_con_que = request.POST['p_con_que'], p_para_que = request.POST['p_para_que'])
	if request.method == "POST" and 'btn_hacer' in request.POST:
		Phva.objects.filter(id_proyecto = 1).update(h_ident_oportunidad = request.POST['h_ident_oportunidad'], h_desarrollo_plan = request.POST['h_desarrollo_plan'], h_implementacion_procesos = request.POST['h_implementacion_procesos'], h_implementacion_mejoras = request.POST['h_implementacion_mejoras'])
	if request.method == "POST" and 'btn_verificar' in request.POST:		
		Phva.objects.filter(id_proyecto = 1).update(v_hizo_lo_planeado = request.POST['v_hizo_lo_planeado'], v_logro_resultados = request.POST['v_logro_resultados'], v_que_eficacia = request.POST['v_que_eficacia'], v_que_impacto = request.POST['v_que_impacto'], v_que_explica_resultados = request.POST['v_que_explica_resultados'])
	if request.method == "POST" and 'btn_actuar' in request.POST:
		Phva.objects.filter(id_proyecto = 1).update(a_que_aprendimos = request.POST['a_que_aprendimos'], a_que_errores = request.POST['a_que_errores'], a_que_aciertos = request.POST['a_que_aciertos'], a_como_cuando_aplicar = request.POST['a_como_cuando_aplicar'])
	lista = Phva.objects.get(id_phva = 2)
	ctx = {'lista':lista}
	return render(request, 'canvas.html', ctx)